#include <QVBoxLayout>
#include <QPushButton>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include "auther.h"
#include "keepassxc/totp/totp.h"

Auther::Auther(QWidget *parent)
    : QWidget(parent)
{
    // get data directory path and create if it doesn't exist
    dataDir = QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    dataDir.mkpath(dataDir.absolutePath());

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dataDir.absolutePath() + QDir::separator() + "data.sqlite");

    QVBoxLayout *centralLayout = new QVBoxLayout();

    ActionBar *actionBar = new ActionBar();
    connect(actionBar, &ActionBar::addAccountSignal, this, &Auther::addAccount);
    connect(actionBar, &ActionBar::unsetDeleteModeSignal, this, &Auther::unsetDeleteMode);
    connect(actionBar, &ActionBar::searchTextUpdatedSignal, this, &Auther::searchTextUpdated);
    connect(actionBar, &ActionBar::setDeleteModeSignal, this, &Auther::setDeleteMode);

    centralLayout->addWidget(actionBar);

    // list of accounts
    QScrollArea *accountsScrollArea = new QScrollArea(this);

#ifdef Q_OS_ANDROID
    accountsScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    accountsScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QScroller::grabGesture(accountsScrollArea->viewport(), QScroller::TouchGesture);

    // disable overshoot
    QScrollerProperties properties = QScroller::scroller(accountsScrollArea->viewport())->scrollerProperties();
    QVariant overshootPolicy = QVariant::fromValue<QScrollerProperties::OvershootPolicy>(QScrollerProperties::OvershootAlwaysOff);
    properties.setScrollMetric(QScrollerProperties::VerticalOvershootPolicy, overshootPolicy);
    properties.setScrollMetric(QScrollerProperties::HorizontalOvershootPolicy, overshootPolicy);
    QScroller::scroller(accountsScrollArea->viewport())->setScrollerProperties(properties);
#endif

    accountsScrollArea->setWidgetResizable(true);
    QWidget *accountsWidget = new QWidget();
    accountsVBox = new QVBoxLayout();
    accountsVBox->addStretch(1);
    accountsWidget->setLayout(accountsVBox);
    accountsScrollArea->setWidget(accountsWidget);

    centralLayout->addWidget(accountsScrollArea);
    setLayout(centralLayout);

    loadAccounts();

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Auther::updateAccounts);
    timer->start(100);
}

void Auther::deleteAccount(Account *account)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Name TEXT, Secret TEXT)");
        query.prepare("DELETE FROM Accounts WHERE Name='" + account->getName() + "'"); 
        query.exec();

        accountsVBox->removeWidget(account);
        delete account;
        accounts.erase(std::remove(accounts.begin(), accounts.end(), account), accounts.end());
        db.close();
    }
}

void Auther::updateAccounts(void)
{
    for (Account *account : accounts) {
        account->update();
    }
}

void Auther::listAccount(Account *account)
{
    accountsVBox->insertWidget(0, account);
}

bool Auther::checkSecret(QString secret)
{
    if (Totp::generateTotp(secret.toLatin1(), std::time(0), 6, 30) == "Invalid TOTP secret key")
        return false;
    else
        return true;
}

void Auther::setDeleteMode()
{
    for (Account *account : accounts) {
        account->setDeleteMode();
    }
}

void Auther::unsetDeleteMode()
{
    for (Account *account : accounts) {
        account->unsetDeleteMode();
    }
}

void Auther::searchTextUpdated(QString text)
{
    for (Account *account : accounts) {
        if (!(account->getName().toLower().contains(text))) {
            account->hide();
        } else {
            account->show();
        }
    }
}

void Auther::addAccount(QString name, QString secret)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        if (checkSecret(secret)) {
            QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Name TEXT, Secret TEXT)");
            query.prepare("INSERT INTO Accounts VALUES('" + name + "','" + secret + "')");
            query.exec();

            Account *account = new Account();
            account->setName(name);
            account->setSecret(secret);

            accounts.push_back(account);
            loadAccounts();
            db.close();
        }
    }
}

void Auther::loadAccounts(void)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        // clear old accounts
        for (std::size_t i = 0; i < accounts.size(); ++i) {
            accountsVBox->removeWidget(accounts[i]);
            delete accounts[i];
        }
        accounts.clear();

        QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Name TEXT, Secret TEXT)");

        query.prepare("SELECT * FROM Accounts");
        query.exec();

        while (query.next()) {
            Account *account = new Account();
            account->setName(query.value(0).toString());
            account->setSecret(query.value(1).toString());
            connect(account, &Account::deleteSignal, this, &Auther::deleteAccount);

            accounts.push_back(account);
            listAccount(account);
        }
        db.close();
    }
}
