#pragma once

#include <QtWidgets>

class Account : public QFrame {
    Q_OBJECT

public:
    Account(QFrame *parent = 0);
    void setSecret(QString secret);
    void setName(QString name);
    QString getName(void);
    QString getTOTP(void);
    void update(void);
    void setDeleteMode(void);
    void unsetDeleteMode(void);

signals:
    void deleteSignal(Account* account);

private:
    void copyClicked(void);
    void deleteClicked(void);
    QPushButton *deleteButton;
    QPushButton *copyButton;
    QString secret;
    QString name;
    QLabel *nameLabel;
    QLabel *tokenLabel;
};
