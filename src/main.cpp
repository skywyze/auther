#include <QApplication>
#include "auther.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);  

    Auther window;

    window.setWindowTitle("auther");
    window.show();

    return app.exec();
}
