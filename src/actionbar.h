#pragma once

#include <QVBoxLayout>
#include <QStackedWidget>
#include <QLineEdit>
#include <QWidget>

class ActionBar : public QStackedWidget {
    Q_OBJECT

public:
    ActionBar(QWidget *parent = 0);

signals:
    void addAccountSignal(QString name, QString secret);
    void setDeleteModeSignal(void);
    void unsetDeleteModeSignal(void);
    void searchTextUpdatedSignal(QString text);

private:
    void addClicked(void);
    void deleteClicked(void);
    void searchClicked(void);
    void addDoneClicked(void);
    void addCancelClicked(void);
    void deleteDoneClicked(void);
    void searchTextChanged(void);
    void searchDoneClicked(void);
    QLineEdit *nameEdit;
    QLineEdit *secretEdit;
    QLineEdit *searchEdit;
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
};
