#pragma once

#include <QVBoxLayout>
#include <QWidget>
#include <QSqlDatabase>
#include <vector>
#include "account.h"
#include "actionbar.h"

class Auther : public QWidget {
public:
    Auther(QWidget *parent = 0);

private:
    void listAccount(Account* account);
    void loadAccounts(void);
    void updateAccounts(void);
    void addAccount(QString name, QString secret);
    void deleteAccount(Account *account);
    bool checkSecret(QString secret);
    void setDeleteMode(void);
    void unsetDeleteMode(void);
    void searchTextUpdated(QString text);
    QVBoxLayout *accountsVBox;
    std::vector<Account*> accounts;
    QDir dataDir;
    QSqlDatabase db;
};
