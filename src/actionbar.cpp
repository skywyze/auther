#include <QVBoxLayout>
#include <QPushButton>
#include "actionbar.h"

ActionBar::ActionBar(QWidget *parent)
    : QStackedWidget(parent)
{
    QWidget *mainBar= new QWidget();

    QPushButton *addButton = new QPushButton("Add");
    connect(addButton, &QPushButton::clicked, this, &ActionBar::addClicked);

    QPushButton *deleteButton = new QPushButton("Delete");
    connect(deleteButton, &QPushButton::clicked, this, &ActionBar::deleteClicked);

    QPushButton *searchButton = new QPushButton("Search");
    connect(searchButton, &QPushButton::clicked, this, &ActionBar::searchClicked);

    QHBoxLayout *mainBarHBox= new QHBoxLayout();
    mainBarHBox->addStretch(1);
    mainBarHBox->addWidget(addButton);
    mainBarHBox->addWidget(deleteButton);
    mainBarHBox->addWidget(searchButton);
    mainBarHBox->addStretch(1);
    mainBar->setLayout(mainBarHBox);

    QWidget *addBar = new QWidget();

    nameEdit = new QLineEdit();
    nameEdit->setPlaceholderText("Name");
    secretEdit = new QLineEdit();
    secretEdit->setPlaceholderText("Secret");

    QPushButton *addDoneButton = new QPushButton("Done");
    connect(addDoneButton, &QPushButton::clicked, this, &ActionBar::addDoneClicked);
    QPushButton *addCancelButton = new QPushButton("Cancel");
    connect(addCancelButton, &QPushButton::clicked, this, &ActionBar::addCancelClicked);

    QHBoxLayout *addBarButtonHBox = new QHBoxLayout();
    addBarButtonHBox->addStretch(1);
    addBarButtonHBox->addWidget(addDoneButton);
    addBarButtonHBox->addWidget(addCancelButton);
    addBarButtonHBox->addStretch(1);

    QVBoxLayout *addBarVBox = new QVBoxLayout();
    addBarVBox->addWidget(nameEdit);
    addBarVBox->addWidget(secretEdit);
    addBarVBox->addLayout(addBarButtonHBox);
    addBar->setLayout(addBarVBox);

    QWidget *deleteBar = new QWidget();
    QPushButton *deleteDoneButton = new QPushButton("Done");
    connect(deleteDoneButton, &QPushButton::clicked, this, &ActionBar::deleteDoneClicked);

    QHBoxLayout *deleteBarHBox = new QHBoxLayout();
    deleteBarHBox->addStretch(1);
    deleteBarHBox->addWidget(deleteDoneButton);
    deleteBarHBox->addStretch(1);
    deleteBar->setLayout(deleteBarHBox);

    QWidget *searchBar = new QWidget();
    searchEdit = new QLineEdit();
    searchEdit->setPlaceholderText("Search");
    searchEdit->setInputMethodHints(Qt::ImhNoPredictiveText);
    connect(searchEdit, &QLineEdit::textChanged, this, &ActionBar::searchTextChanged);
    QPushButton *searchDoneButton = new QPushButton("Done");
    connect(searchDoneButton, &QPushButton::clicked, this, &ActionBar::searchDoneClicked);

    QHBoxLayout *searchBarHBox = new QHBoxLayout();
    searchBarHBox->addWidget(searchEdit);
    searchBarHBox->addWidget(searchDoneButton);
    searchBar->setLayout(searchBarHBox);

    // toolbar at top
    addWidget(mainBar);
    addWidget(addBar);
    addWidget(deleteBar);
    addWidget(searchBar);
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
}

void ActionBar::addCancelClicked(void)
{
    nameEdit->clear();
    secretEdit->clear();
    setCurrentIndex(0);
}

void ActionBar::addDoneClicked(void)
{
    QString name = nameEdit->text();
    QString secret = secretEdit->text();
    emit addAccountSignal(name, secret);
    nameEdit->clear();
    secretEdit->clear();
    setCurrentIndex(0);
}

void ActionBar::deleteDoneClicked(void)
{
    emit unsetDeleteModeSignal();
    setCurrentIndex(0);
}

void ActionBar::searchTextChanged(void)
{
    emit searchTextUpdatedSignal(searchEdit->text().toLower());
}

void ActionBar::searchDoneClicked(void)
{
    searchEdit->clear();
    setCurrentIndex(0);
}

void ActionBar::addClicked(void)
{
    setCurrentIndex(1);
}

void ActionBar::deleteClicked(void)
{
    emit setDeleteModeSignal();
    setCurrentIndex(2);
}

void ActionBar::searchClicked(void)
{
    setCurrentIndex(3);
}

QSize ActionBar::minimumSizeHint() const
{
    return currentWidget()->minimumSizeHint();
}

QSize ActionBar::sizeHint() const
{
    return currentWidget()->sizeHint();
}
