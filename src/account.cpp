#include <ctime>
#include "account.h"
#include "keepassxc/totp/totp.h"

Account::Account(QFrame *parent)
    : QFrame(parent)
{
    setFrameShape(QFrame::StyledPanel);

    this->tokenLabel = new QLabel();
    this->nameLabel = new QLabel();

    // increase tokenLabel font size
    QFont font = tokenLabel->font();
    if (font.pixelSize() != -1) {
        font.setPixelSize(1.75 * font.pixelSize());
    } else {
        font.setPointSize(1.75 * font.pointSize());
    }
    tokenLabel->setFont(font);

    copyButton = new QPushButton("Copy");
    connect(copyButton, &QPushButton::clicked, this, &Account::copyClicked);

    deleteButton = new QPushButton("Delete");
    connect(deleteButton, &QPushButton::clicked, this, &Account::deleteClicked);
    deleteButton->hide();

    QHBoxLayout *hbox = new QHBoxLayout();
    QVBoxLayout *labelBox = new QVBoxLayout();

    labelBox->addWidget(tokenLabel);
    labelBox->addWidget(nameLabel);

    hbox->addLayout(labelBox);
    hbox->addStretch(1);
    hbox->addWidget(copyButton);
    hbox->addWidget(deleteButton);

    setLayout(hbox);
    setMaximumHeight(minimumSizeHint().height());
}

void Account::copyClicked()
{
    QApplication::clipboard()->setText(tokenLabel->text());
}

void Account::deleteClicked()
{
    emit deleteSignal(this);
}

void Account::setDeleteMode(void)
{
    copyButton->hide();
    deleteButton->show();
}

void Account::unsetDeleteMode(void)
{
    deleteButton->hide();
    copyButton->show();
}

void Account::update(void)
{
    this->tokenLabel->setText(getTOTP());
}

void Account::setSecret(QString secret)
{
    this->secret = secret;
    this->tokenLabel->setText(getTOTP());
}

void Account::setName(QString name)
{
    this->name = name;
    this->nameLabel->setText(name);
}

QString Account::getName(void)
{
    return this->name;
}

QString Account::getTOTP(void)
{
    return Totp::generateTotp(secret.toLatin1(), std::time(0), 6, 30);
}
